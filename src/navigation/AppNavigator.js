import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListScreen from '../screens/ListScreen'

const Stack = createStackNavigator()
function MainStackNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='Calculator' component={ListScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default MainStackNavigator