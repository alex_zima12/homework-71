import React from 'react'
import {StyleSheet, Text, View, StatusBar, SafeAreaView} from "react-native";
import {useSelector, useDispatch} from "react-redux";
import Row from "../../components/Row/Row";
import Button from "../../components/Button/Button";

function ListScreen() {

    const result = useSelector(state => state.value);
    const dispatch = useDispatch();
    const getResult = () => dispatch({type: "RESULT"})
    const clearValue = () => dispatch({type: "CLEAR"})
    const deleteLastSymbol = () => dispatch({type: "DELETE_LAST_SYMBOL"})
    const onClickHandler = (val) => dispatch({type: "ADD", symbol: val})
    const onClickHandlerOperation = (val) => dispatch({type: "ADD_OPERATION", symbol: val})

    return (

        <View style={styles.container}>
            <StatusBar barStyle="light-content"/>
            <SafeAreaView>
                <Text style={styles.value}>
                    {result}
                </Text>
                <Row>
                    <Button
                        text="C"
                        theme="secondary"
                        onPress = {clearValue}
                    />
                    <Button
                        text=">"
                        theme="secondary"
                        onPress = {deleteLastSymbol}
                    />
                    <Button
                        theme="secondary"
                    />
                    <Button
                        text="/"
                        theme="accent"
                        onPress = { () => onClickHandlerOperation("/")}
                    />
                </Row>

                <Row>
                    <Button text="7"
                            onPress = { () => onClickHandler(7)}
                    />
                    <Button text="8"
                            onPress = { () => onClickHandler(8)}
                    />
                    <Button text="9"
                            onPress = { () => onClickHandler(9)}
                    />
                    <Button
                        text="*"
                        theme="accent"
                        onPress = { () => onClickHandlerOperation("*")}
                    />
                </Row>
                <Row>
                    <Button text="4"
                            onPress = { () => onClickHandler(4)}
                    />
                    <Button text="5"
                            onPress = { () => onClickHandler(5)}
                    />
                    <Button text="6"
                            onPress = { () => onClickHandler(6)}
                    />
                    <Button
                        text="-"
                        theme="accent"
                        onPress = { () => onClickHandlerOperation("-")}
                    />
                </Row>
                <Row>
                    <Button text="1"
                            onPress = { () => onClickHandler(1)}
                    />
                    <Button text="2"
                            onPress = { () => onClickHandler(2)}
                    />
                    <Button text="3"
                            onPress = { () => onClickHandler(3)}
                    />
                    <Button
                        text="+"
                        theme="accent"
                        onPress = { () => onClickHandlerOperation("+")}
                    />
                </Row>

                <Row>
                    <Button
                        text="0"
                        size="double"
                        onPress = { () => onClickHandler(0)}
                    />
                    <Button text="."
                            onPress = { () => onClickHandler(".")}
                    />
                    <Button
                        text="="
                        theme="accent"
                        onPress = {getResult}
                    />
                </Row>
            </SafeAreaView>
        </View>

    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#202020",
        justifyContent: "flex-end"
    },
    value: {
        color: "#fff",
        fontSize: 40,
        textAlign: "right",
        marginRight: 20,
        marginBottom: 10
    }
});

export default ListScreen