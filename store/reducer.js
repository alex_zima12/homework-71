const initialState = {
    value: '',
    flag: false
};

const reducer = (state= initialState,action) => {
    switch (action.type) {
        case 'ADD':
            return {...state,  value: (state.flag ? action.symbol:(state.value + action.symbol.toString())), flag:false}
        case 'ADD_OPERATION':
            return {...state,  value:  state.value + action.symbol.toString(), flag:false}
        case 'RESULT':
            return {...state,  value:eval(state.value).toString(), flag:true }
        case 'CLEAR':
            return {...state, value: '',flag:false}
        case 'DELETE_LAST_SYMBOL':
            return {...state, value: state.value.substr(0,state.value.length-1),flag:false}
        default:
            return state;
    };
};

export default reducer;